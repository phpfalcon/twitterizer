﻿namespace Twitterizer.RateLimitStatus
	
{
	using System;
    using Newtonsoft.Json;
    using System.Runtime.Serialization;    
    using Twitterizer.Core;
    
    /// <summary>
    /// The Searches of rate limit status.
    /// </summary>
    /// 
#if !SILVERLIGHT
    [Serializable]
#endif    
   [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class RateLimitSearch: TwitterObject
    {
        #region API Properties
        
        /// <summary>
        /// Gets or sets the search tweets.
        /// </summary>
        [DataMember, JsonProperty(PropertyName = "/search/tweets")]
        public TwitterRateLimitResults Tweets { get; set; }
        
        #endregion
	}
}